import Vue from "vue";
import Router from "vue-router";
import AppHeader from "../layout/AppHeader";
import AppFooter from "../layout/AppFooter";
import Components from "../views/Components";
import Contact from "../views/Contact";
import Blog from "../views/Blog";
import Article from "@/views/components/Article/Article";

Vue.use(Router);

export default new Router({
  linkExactActiveClass: "active",
  mode: "history",
  routes: [
    {
      path: "/",
      name: "Home",
      components: {
        header: AppHeader,
        default: Components,
        footer: AppFooter
      }
    },
    {
      path: "/blog",
      name: "Blog",
      components: {
        header: AppHeader,
        default: Blog,
        footer: AppFooter
      }
    },
    {
      path: "/blog/:slug",
      name: "Article",
      components: {
        header: AppHeader,
        default: Article,
        footer: AppFooter
      }
    },
    {
      path: "/contacto",
      name: "Contacto",
      components: {
        header: AppHeader,
        default: Contact,
        footer: AppFooter
      }
    },
    // {
    //   path: "/profile",
    //   name: "profile",
    //   components: {
    //     header: AppHeader,
    //     default: Profile,
    //     footer: AppFooter
    //   }
    // }
  ],
  scrollBehavior: to => {
    if (to.hash) {
      return { selector: to.hash };
    } else {
      return { x: 0, y: 0 };
    }
  }
});
